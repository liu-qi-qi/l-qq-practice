// 第一题得到对象扁平化之后的结果
// 实现对象扁平化
const obj={
    a: {
        b: 1,
        c: 2,
        d: {
            e: 5
        }
     },
    b: [ 1,  3,  {a: 2,  b: 3} ],
    c: 3,
    d: [{
        c: 'd-c',
        d: {
          e: 'd-d-e'
        }
}]
}
// console.log(obj),
function flatten(obj) {
    const result = {};
    // 定义递归函数
    function dealFn(k, v) {
      // 判断传递过来的数据类型
      if (Object(v) !== v) {
        // 证明是普通数据类型，那么直接复制
        result[k] = v;
        // 判断传递的数据是不是一个数组
      } else if (Array.isArray(v)) {
        // 判断数组的长度
        if (!v.length) {
          // 如果数长度 <=0 那么证明数组为一个空数组
          result[k] = [];
        } else {
          // 遍历数组
          v.forEach((item, index) => {
            // 调用递归函数 递归处理
            dealFn(`${k}[${index}]`, item);
          });
        }
        // 否则就是对象
      } else {
        // 拿到对象内的所有key
        const keys = Object.keys(v);
        // 判断对象内成员的长度
        if (!keys.length) {
          result[k] = {};
        } else {
          keys.forEach((item) => {
            dealFn(k ? `${k}.${item}` : item, v[item]);
          });
        }
      }
    }
    // 第一次调用 没有传递key的值
    dealFn('', obj);
    // 返回结果
    return result;
    }
console.log(flatten(obj))
    
   

// const resoult = flatten({
//    a: {
//         b: 1,
//         c: 2,
//         d: {
//             e: 5
//         }
//      },
//     b: [ 1,  3,  {a: 2,  b: 3} ],
//     c: 3,
//     d: [{
//         c: 'd-c',
//         d: {
//           e: 'd-d-e'
//         }
//   }]
//  });

// console.log(resoult)
// {
//     "a.b": 1,
//     "a.c": 2,
//     "a.d.e": 5,
//     "b[0]": 1,
//     "b[1]": 3,
//     "b[2].a": 2,
//     "b[2].b": 3,
//     "c": 3,
//     "d[0].c": "d-c",
//     "d[0].d.e": "d-d-e"
//   }



// 第二题：由该数组得到 [5,[[4,3],2,1]] =》 (5 - ((4 - 3) - 2 - 1)) 的值

// function getResoult(arr){
    // your code
// },
// console.log(getResoult( [5,[[4,3],2,1]] ))
//  (5 - ((4 - 3) - 2 - 1))结果
