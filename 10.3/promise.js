
const PADDING = 'PADDING';
const FULLFILD = 'FULLFILD';
const REJECT = 'REJECT';
class Promise {
  constructor(callback){
    this.status = PADDING;
    this.resolve = function(data){ //异步的
      this.status = FULLFILD;
      this.resolveCk(data);
    }
    this.reject = function(error){ // 异步
      this.status = REJECT;
      if(this.rejectCk){
        this.rejectCk(error)
        return
      }
      if(this.catchCk) {
        this.catchCk(error)
      }
    }
    callback(this.resolve, this.reject)
  }
  then(resolveCk, rejectCk){
    this.resolveCk = resolveCk;
    this.rejectCk = rejectCk;
    return new Promise()
  }
  catch(ck){
    this.catchCk = ck;
    return new Promise()
  }
}

new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(1)
  })
}).then(res => {
  console.log(res);
})
