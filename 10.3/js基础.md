js基础补充 分别说出理解
一、promise
Promise是JS异步编程中的重要概念，异步抽象处理对象，是目前比较流行Javascript异步编程解决方案之一，Promise 是一个构造函数， new Promise 返回一个 promise对象 接收一个excutor执行函数作为参数, excutor有两个函数类型形参resolve reject

promise的三种状态（1.pending 2.fulfilled 3.rejected【一旦修改就不能再变】
                1.promise 对象初始化状态为 pending
                2.当调用resolve(成功)，会由pending => fulfilled   onFulfilled会被调用
                3.当调用reject(失败)，会由pending => rejected   onRejected会被调用

promise的三个实例方法
then方法 注册 当resolve(成功)/reject(失败)的回调函数 then方法是异步执行的,返回的是一个新的Promise实例
catch方法  在链式写法中可以捕获前面then中发送的异常
finally()方法  指定不管 Promise 对象最后状态如何，都会执行的操作

promise的静态方法：
        resolve方法：
                    如果这个值是一个 promise ，那么将返回这个 promise
                    参数不是具有then()方法的对象，或根本就不是对象，Promise.resolve()会返回一个新的 Promise 对象，状态为resolved
                    没有参数时，直接返回一个resolved状态的 Promise 对象
        reject()方法
                    Promise.reject(reason)方法也会返回一个新的 Promise 实例,该实例的状态为rejected
        any()方法
                    接收一个Promise可迭代对象，只要其中的一个 promise 成功，就返回那个已经成功的 promise 。如果可迭代对象中没有一个 promise 成功（即所有的 promises 都失败/拒绝），就返回一个失败的 promise 和AggregateError类型的实例，它是 Error 的一个子类，用于把单一的错误集合在一起。本质上，这个方法和Promise.all()是相反的。
        All()方法
                    Promise.all 会在任何一个请求失败的时候进入失败状态由于单一 Promise 进入 rejected 状态便会立即让 Promise.all() 的结果进入 rejected 状态，以至于通过 Promise.all() 进入 rejected 状态时，其中的源 Promise 仍然可能处于 pending 状态，以至于无法获得所有 Promise 完成的时机。方法接收一个promise的iterable类型，并且只返回一个Promise实例
                     例：const p = Promise.all([p1, p2, p3]);
                     实例p的状态由p1,p2,p3决定，分两种情况：
                        1.只有p1、p2、p3的状态都变成fulfilled，p的状态才会变成fulfilled，此时p1、p2、p3的返回值组成一个数组，传递给p的回调函数
                        2.只要p1、p2、p3之中有一个被rejected，p的状态就变成rejected，此时第一个被reject的实例的返回值，会传递给p的回调函数
        allSettled方法
                    Promise.allSettled() 静态方法会等待所有源 Promise 进入 fulfilled 或者 rejected 状态，从而确保不会造成时序上的冲突。
        race方法
                    将多个 Promise 实例，包装成一个新的 Promise 实例

        
二、async
async 可以单独存在, 用该关键字声明的函数返回的是一个 Promise 对象。
await 不可以单独存在，必须要配合 async 一起存在使用
async 函数可能包含 0 个或者多个await表达式。await 表达式会暂停整个 async 函数的执行进程并出让其控制权，只有当其等待的基于 promise 的异步操作被兑现或被拒绝之后才会恢复进程。promise 的解决值会被当作该 await 表达式的返回值。使用async / await关键字就可以在异步代码中使用普通的try / catch代码块。

三、eventloop 【事件循环机制】
执行栈在执行完同步任务后，查看执行栈是否为空，如果执行栈为空，就会去执行Task（宏任务），每次宏任务执行完毕后，检查微任务(microTask)队列是否为空，如果不为空的话，会按照先入先出的规则全部执行完微任务(microTask)后，设置微任务(microTask)队列为null，然后再执行宏任务，如此循环。
先执行微任务，在执行宏任务。
微任务：Propmise  async/await
宏任务：setTimeout  setInterval  ajax  Dom事件